#include <iostream>
#include "sort.h"

sort::sort(int b[], int s) {
    size = s;
    for (int i = 0; i < size; i++) {
        a[i] = b[i];
    }
}

sort::~sort() = default;

void sort::print() {
    for (int i = 0; i < size; i++) {
        std::cout << a[i] << " ";
    }
}

void sort::swap(int i, int j) {
    int temp = a[i];
    a[i]=a[j];
    a[j]=temp;
}

void sort::heapify(int n, int i) {
    int max = i;
    int l = 2*i + 1;
    int r = 2*i + 2;
    /* Wersja z przesunieciem bitowym:
     *  unsigned int l = i<<1 + 1;
     *  unsigned int r = i<<1 + 2;
     */

    if (l < n && a[l] > a[i])
        max = l;
    else
        max = i;

    if (r < n && a[r] > a[max])
        max = r;

    if (max != i) {
        swap(i, max);
        heapify(n, max);
    }
}

void sort::buildHeap(){
    for (int i = (size/2-1);i>=0;i--)
        heapify(size, i);
}

void sort::heapSort() {
    for (int i=size-1;i>0;i--) {
        swap(0, i);
        heapify(i, 0);
    }
}

bool sort::verify() {
    for (int i=1;i<size;i++) {
        if (a[i] < a[i-1]) return false;
    }
    return true;
}