#pragma once

#ifndef sort_h
#define sort_h

const int max_size = 256;

class sort {
private:
    int a[max_size]{};	// tablica do posortowania
    int size;			// rozmiar tablicy

public:
    explicit sort(int b[] =  nullptr , int size = 1);	// konstruktor
    ~sort();
    void print();	// wypisanie tablicy
    void heapSort();	// metoda sortujaca
    void buildHeap();
    void heapify(int n, int i);
    void swap(int i, int j);
    bool verify();	// metoda sprawdzajaca
};

#endif